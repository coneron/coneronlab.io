# Coneron-front-end

Coneron-front-end repository includes Vue.js based front-end of bid management platform for construction industry.

### WIP Demo:[coneron.gitlab.io](http://coneron.gitlab.io)

Sign in with the username "test1@test.com" and the password "test".

#### Backend repository
- [Coneron-back-end](https://gitlab.com/coneron/coneron-back-end)

## Front-end

- **[Vue.js](https://vuejs.org/)** and **[Vuex](https://github.com/vuejs/vuex)**
- **[Bootstrap](http://getbootstrap.com)**
- **[Vue-router](https://github.com/vuejs/vue-router)** 
- **[Vuex-router-sync](https://github.com/vuejs/vuex-router-sync)** 
- **[Vuetable-2](https://github.com/ratiw/vuetable-2)** 
- **[Vuikit](https://github.com/vuikit/vuikit)** - Only minor usage

### Continuous integration
- Gitlab CI deploying to Gitlab pages [docs](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/)

#### Vue-2.0-boilerplate Documentation

This project was bootstrapped with [Vue-2.0-boilerplate](https://github.com/petervmeijgaard/vue-2.0-boilerplate).

Refer vue-2.0-boilerplate guide how to perform common tasks.<br>
You can find the most recent version of guide [here](https://github.com/petervmeijgaard/vue-2.0-boilerplate).

