/* ============
 * Button Component
 * ============
 *
 * todo: add documentation here!
 */
import * as services from './../../services';

export default {
  data() {
    return {
      activeVal: this.active,
    };
  },
  name: 'VButton',
  props: {
    value: {},
    type: {
      type: String,
      default: 'button',
    },
    ariaType: {
      type: String,
      default: 'pressed',
    },
    active: {
      type: Boolean,
      default: false,
    },
    disabled: {
      type: Boolean,
      default: false,
    },
     // class related
    color: {
      type: String,
      default: '',
    },
    size: {
      type: String,
      default: '',
    },
    width: {
      type: String,
      default: '',
    },
    item: {
      default: '',
    },
    selected: {
      default: '',
    },
  },
  methods: {
    //  getState(store) {
    //    return store.state.quote.service[this.name];
    //  },
    updateQuote(id) {
      this.activeVal = !this.activeVal;
      console.log(id);
      this.$store.dispatch('updateQuoteCategories', id);

      //  if (!this.color) {
      //    // this.selected = e.target.value;
      //   //  quoteService.update(e.target.value, 'category');
      //   //  quoteService.update(null, 'subCategory');
      //  } else {
      //    // this.selected = e.target.value;
      //    // this.active = !this.active;
      //   //  quoteService.update(e.target.value, 'subCategory');
      //  }
    },
  },
  computed: {
    checkValue() {
      if (Number(this.selected) === Number(this.value)) {
        return true;
      }
      return false;
    },
  },
};
