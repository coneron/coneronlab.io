/* ============
 * Cost-row-component
 * ============
 *
 * todo: add documentation here!
 */

export default {
  data() {
    return {
      rows: [
        {
          id: '1',
          description: '',
          quantity: '',
        },
      ],
    };
  },
  methods: {
    addRow() {
      this.rows.push({ id: '', description: '', quantity: '' });
    },
    removeRow(row) {
      // console.log(row);
      this.rows.splice(row, 1);
    },
  },
};
