/* ============
 * Component: form-group-input
 * ============
 *
 * Generated component.
 *
 * Describe your component here.
 */
// import quoteService from './../../services/quote';


export default {
  data() {
    return {
      amount: null,
    };
  },
  props: {
    component: {
      type: String,
      default: '',
    },
    name: {
      type: String,
      default: '',
    },
    rules: {
      type: String,
      default: '',
    },
    type: {
      type: String,
      default: 'text',
    },
    placeholder: {
      type: String,
      default: '',
    },
    contextualStyle: {
      type: String,
      required: false,
    },
    source: {
      type: String,
      default: '',
    },
  },
  methods: {
    getState(store) {
      if (this.source === 'offer') {
        return store.state.offer.current[this.name];
      } else if (this.source === 'organization') {
        return this.$store.state.organization.current[this.name];
      } else if (this.source === 'account') {
        return this.$store.state.account[this.name];
      } else if (this.source === 'project') {
        return this.$store.state.project.current[this.name];
      }
      return store.state.quote.current[this.name];
    },
    updateQuote(e) {
      const updateArr = [e.target.value, e.target.name];
      const contactsArr = [e.target.value];

      if (this.source === 'account') {
        this.$store.dispatch('updateAccount', updateArr);
      } else if (this.source === 'organization') {
        this.$store.dispatch('updateOrganization', updateArr);
      } else if (this.source === 'newProject') {
        this.$store.dispatch('createProject', updateArr);
      } else if (this.source === 'project') {
        this.$store.dispatch('updateProject', updateArr);
      } else if (this.source === 'offer') {
        this.$store.dispatch('updateOffer', updateArr);
      } else if (this.source === 'contacts') {
        this.$store.dispatch('updateContacts', contactsArr);
      } else {
        this.$store.dispatch('updateQuote', updateArr);
      }
    },
    doesHaveError() {
      if (this.errors) {
        if (this.errors.has(this.name)) {
          return true;
        }
      }
      return false;
    },
    renderError() {
      return this.errors.first(this.name);
    },
    checkComponentType(type) {
      if (this.component === type) {
        return true;
      }
      return false;
    },
  },
  computed: {
    classNames() {
      const classNames = ['form-control'];

      if (this.contextualStyle) {
        classNames.push(`${this.contextualStyle}`);
      }

      return classNames;
    },
  },
};
