/* ============
 * Load-data Component
 * ============
 *
 * todo: add documentation here!
 */

export default {
  data() {
    return {
      loading: true,
      error: null,
    };
  },
  created() {
    this.fetchData();
  },
  methods: {
    fetchData() {
      if (this.$store.state.route.params.quoteId) {
        this.$store.dispatch('getQuote', this.$store.state.route.params.quoteId).then(() => {
          this.loading = false;
        });
      }
    },
  },
};
