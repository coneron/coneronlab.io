/* ============
 * Modal Component
 * ============
 *
 * todo: add documentation here!
 */

export default {
  components: {
    VFormGroupInput: require('components/form-group-input/form-group-input.vue'),
  },
  props: {
    modalShow: {
      type: Boolean,
      default: false,
    },
    modalHeader: {
      type: String,
      default: '',
    },
    modalConfirm: {
      type: String,
      default: 'Kyllä',
    },
  },
};
