/* ============
 * Nav-footer Component
 * ============
 *
 * todo: add documentation here!
 */

import * as services from './../../services';
import { router } from './../../../bootstrap';

export default {
  data: () => ({
    show: false,
    showAfterSending: false,
  }),
  props: {
    prevRoute: {
      type: String,
      default: '',
    },
    nextRoute: {
      type: String,
      default: '',
    },
    routeParams: {
      type: Object,
      default: '',
    },
    type: {
      type: String,
      default: '',
    },
  },
  methods: {
    saveQuote() {
      this.show = false;

      const projectId = parseInt(this.$store.state.route.params.projectId, 10);
      const quoteId = parseInt(this.$store.state.route.params.quoteId, 10);

      const updateArr = [projectId, 'projectId'];

      this.$store.dispatch('updateQuote', updateArr).then(() => {
        services.quoteService.patch(quoteId, this.$store.state.quote.current);
        services.mailQuoteService.create(this.$store.state.quote.current);
        this.showAfterSending = true;
        // router.push({ name: this.nextRoute, params: this.routeParams });
      });
    },
  },
};
