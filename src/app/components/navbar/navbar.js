/* ============
 * Navbar Component
 * ============
 *
 * todo: add documentation here!
 */

import authService from './../../services/auth';

export default {
  methods: {
    logout() {
      authService.logout();
    },
  },
};
