/* ============
 * Vuetable-container Component
 * ============
 *
 * todo: add documentation here!
 */
import * as moment from 'moment';
import 'moment/locale/fi';

// TODO: Support other locales
moment.locale('fi');

export default {
  mixins: [noDataMixin],
  components: {
    Vuetable,
    VuetablePagination,
    VModal: require('components/modal/modal.vue'),
    VAppLayout: require('layouts/app/app.vue'),
    VSubHeader: require('components/subheader/subheader.vue'),
    VVuetableContainer: require('components/vuetable-container/vuetable-container.vue'),
    VOpenQuotesVuetable: require('components/open-quotes-vuetable/open-quotes-vuetable.vue')
    VSpinner: require('components/spinner/spinner.vue'),
    VBloc: require('components/bloc/bloc.vue'),
    VSubnavProject: require('components/subnav-project/subnav-project.vue'),
  },
  data: () => ({
    url: process.env.API_LOCATION,
    css: BootstrapStyle,
    params: { sort: '$sort', page: '', perPage: '$limit' },
    index: 0,
    fields: [
      {
        name: '__slot:quoteName',
        title: 'Tarjouspyyntö',
        dataClass: 'p-t-2',
        callback: 'formatQuoteName',
      },
      {
        name: 'category',
        title: 'Kategoria',
        dataClass: 'p-t-2',
        callback: 'formatEmpty',
      },
      {
        name: 'quoteEndDate',
        title: 'Umpeutuminen',
        dataClass: 'p-t-2',
        callback: 'formatDate',
      },
      {
        name: 'offers',
        title: 'Tarjoukset',
        dataClass: 'p-t-2',
        callback: 'formatEmpty',
      },
      {
        name: '__slot:actions',
        title: 'Toiminnot',
        titleClass: 'text-right',
      },
    ],
  }),
};
