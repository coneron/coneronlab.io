/* ============
 * Quote-preview-body Component
 * ============
 *
 * todo: add documentation here!
 */
import moment from 'moment';

import * as services from './../../services';


export default {
  components: {
    VQuotePreviewSection: require('components/quote-preview-section/quote-preview-section.vue'),
    VPanel: require('components/panel/panel.vue'),
    VSpinner: require('components/spinner/spinner.vue'),
  },
  data: () => ({
    loading: true,
  }),
  created() {
    this.findFiles();
  },
  methods: {
    findFiles() {
      const quoteId = Number(this.$store.state.route.params.quoteId);
      this.$store.dispatch('findFiles', quoteId).then(() => {
        this.loading = false;
      });
    },
    removeFile(fileId) {
      this.$store.dispatch('removeFile', fileId);
    },
    formFileUrl(file) {
      return `${process.env.API_LOCATION}/temps/${file}`;
    },
    getValue(name) {
      // TODO: Update based on source
      let value = this.$store.state.quote.current[name];

      if (name === 'quoteEndDate') {
        value = this.formatDate(value);
      }

      if (!value || value === 'default') {
        value = '-';
        if (name === 'quoteName') {
          value = 'Nimetön tarjouspyyntö';
        }
        if (name === 'organizationName') {
          value = 'Nimetön organisaatio';
        }
      }

      return value;
    },
    formatDate(value, fmt = 'D.MM.YYYY') {
      return (value == null)
        ? ''
        : moment(value, 'YYYY-MM-DD').format(fmt);
    },
  },
};
