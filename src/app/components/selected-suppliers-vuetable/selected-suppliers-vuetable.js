/* ============
 * Vuetable-container Component
 * ============
 *
 * todo: add documentation here!
 */
import * as moment from 'moment';
import Vuetable from 'vuetable-2/src/components/Vuetable';
import VuetablePagination from 'vuetable-2/src/components/VuetablePagination';
import BootstrapStyle from './../../mixins/vuetable-bootstrap-css';
import noDataMixin from './../../mixins/no-data-template';

export default {
  mixins: [noDataMixin],
  components: {
    Vuetable,
    VuetablePagination,
    VModal: require('components/modal/modal.vue'),
    VAppLayout: require('layouts/app/app.vue'),
    VSubHeader: require('components/subheader/subheader.vue'),
    VVuetableContainer: require('components/vuetable-container/vuetable-container.vue'),
    VSpinner: require('components/spinner/spinner.vue'),
    VBloc: require('components/bloc/bloc.vue'),
    VSubnavProject: require('components/subnav-project/subnav-project.vue'),
  },
  data: () => ({
    url: process.env.API_LOCATION,
    params: { sort: '$sort', page: '', perPage: '$limit' },
    css: BootstrapStyle,
    index: 0,
    loading: true,
    fields: [
      {
        name: '__slot:quoteName',
        title: 'Tarjouspyyntö',
        dataClass: 'p-t-2',
        callback: 'formatQuoteName',
      },
      {
        name: 'category',
        title: 'Kategoria',
        dataClass: 'p-t-2',
        callback: 'formatEmpty',
      },
      {
        name: 'quoteEndDate',
        title: 'Umpeutuminen',
        dataClass: 'p-t-2',
        callback: 'formatDate',
      },
      {
        name: 'offers',
        title: 'Tarjoukset',
        dataClass: 'p-t-2',
        callback: 'formatEmpty',
      },
      {
        name: '__slot:actions',
        title: 'Toiminnot',
        titleClass: 'text-right',
      },
    ],
  }),
  methods: {
    onLoaded() {
      this.loading = false;
    },
    formLinkToQuote(rowData, name) {
      return { name, params: { projectId: this.$store.state.route.params.projectId, quoteId: rowData.id, quoteName: rowData.quoteName } };
    },
    formatQuoteName(value) {
      if (value) {
        return value;
      }
      return 'Nimetön tarjouspyyntö';
    },
  }
};
