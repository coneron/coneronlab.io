/* ============
 * Vuetable-container Component
 * ============
 *
 * todo: add documentation here!
 */
import * as moment from 'moment';
import 'moment/locale/fi';

// TODO: Support other locales
moment.locale('fi');

export default {
  methods: {
    formatEmpty(value) {
      if (value || value === 0) {
        return value;
      }
      return '-';
    },
    formatDate(value, fmt = 'L') {
      return (value == null)
        ? '-'
        : moment(value, 'YYYY-MM-DD').format(fmt);
    },
    formatQuoteName(value) {
      if (value) {
        return value;
      }
      return 'Nimetön tarjouspyyntö';
    },
  },
};
