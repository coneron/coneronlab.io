/* ============
 * App-inner-nav Layout
 * ============
 *
 * todo: add documentation here!
 */

export default {
  components: {
    vLoadData: require('components/load-data/load-data.vue'),
    VInnerNavbar: require('components/inner-navbar/inner-navbar.vue'),
  },
};
