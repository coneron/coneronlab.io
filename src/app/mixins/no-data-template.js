export default {
  methods: {
    noDataTemplate(loading, noDataString = 'Ei tuloksia.') {
      return (loading) ? '' : noDataString;
    },
  },
};
