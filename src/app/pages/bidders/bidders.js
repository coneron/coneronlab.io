/* ============
 * Bidders Page
 * ============
 *
 * todo: add documentation here!
 */
// import InputTag from 'vue-input-tag';

export default {
  data() {
    return {
      tagsArray: [],
    };
  },
  components: {
    InputTag: require('components/form-input-contacts/form-input-contacts.vue'),
    VAppLayout: require('layouts/app-inner-nav/app-inner-nav.vue'),
    VNavFooter: require('components/nav-footer/nav-footer.vue'),
    VBloc: require('components/bloc/bloc.vue'),
    VFormGroupInput: require('components/form-group-input/form-group-input.vue'),
    VAlert: require('components/alert/alert.vue'),
    VBiddersComponent: require('components/bidders-component/bidders-component.vue')
  },
  methods: {
    callbackMethod() {
      this.$store.dispatch('updateContacts', this.tagsArray);
    },
  },
};
