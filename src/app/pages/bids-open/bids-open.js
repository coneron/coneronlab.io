/* ============
 * Bids-open Page
 * ============
 *
 * todo: add documentation here!
 */
import Vue from 'vue';
import Vuetable from 'vuetable-2/src/components/Vuetable';
import VuetablePagination from 'vuetable-2/src/components/VuetablePagination';
import * as services from './../../services';
import { router } from './../../../bootstrap';
import BootstrapStyle from './../../mixins/vuetable-bootstrap-css';
import noDataMixin from './../../mixins/no-data-template';

export default {
  mixins: [noDataMixin],
  components: {
    Vuetable,
    VuetablePagination,
    VModal: require('components/modal/modal.vue'),
    VAppLayout: require('layouts/app/app.vue'),
    VSubHeader: require('components/subheader/subheader.vue'),
    VVuetableContainer: require('components/vuetable-container/vuetable-container.vue'),
    VSelectedSuppliersVuetable: require('components/selected-suppliers-vuetable/selected-suppliers-vuetable.vue'),
    VSpinner: require('components/spinner/spinner.vue'),
    VBloc: require('components/bloc/bloc.vue'),
    VButton: require('components/button/button.vue'),
    VSubnavProject: require('components/subnav-project/subnav-project.vue'),
  },
  data: () => ({
    url: process.env.API_LOCATION,
    css: BootstrapStyle,
    params: { sort: '$sort', page: '', perPage: '$limit' },
    index: 0,
    loading: true,
    isRemoveModalShown: false,
    isCategoryModalShown: false,
    selectedRow: null,
    fields: [
      {
        name: '__slot:quoteName',
        title: 'Tarjouspyyntö',
        dataClass: 'p-t-2',
        callback: 'formatQuoteName',
      },
      {
        name: 'category',
        title: 'Kategoria',
        dataClass: 'p-t-2',
        callback: 'formatEmpty',
      },
      {
        name: 'quoteEndDate',
        title: 'Umpeutuminen',
        dataClass: 'p-t-2',
        callback: 'formatDate',
      },
      {
        name: 'offers',
        title: 'Tarjoukset',
        dataClass: 'p-t-2',
        callback: 'formatEmpty',
      },
      {
        name: '__slot:actions',
        title: 'Toiminnot',
        titleClass: 'text-right',
      },
    ],
    moreParams: {},
    filterText: '',
  }),
  created() {
    this.$store.dispatch('getProject', this.$store.state.route.params.projectId).then(() => {
      this.loading = false;
    });
    this.getCategories();
  },
  watch: {
    // whenever question changes, this function will run
    filterText() {
      this.moreParams.filter = this.filterText;
      console.log(this.moreParams.filter);
      Vue.nextTick(() => this.$refs.quoteRequestsTable.refresh());
    },
  },

  methods: {
    getCategories() {
      this.$store.dispatch('findCategories');
    },
    doFilter() {
      this.moreParams.filter = this.filterText;
      Vue.nextTick(() => this.$refs.quoteRequestsTable.refresh());
    },
    onLoaded() {
      this.loading = false;
    },
    onPaginationData(paginationData) {
      this.$refs.pagination.setPaginationData(paginationData);
      this.$refs.paginationInfo.setPaginationData(paginationData);
    },
    onChangePage(page) {
      this.$refs.quoteRequestsTable.changePage(page);
    },
    createQuote() {
      services.quoteService.create({ projectId: parseInt(this.$store.state.route.params.projectId, 10) })
        .then((quote) => {
          router.push({ name: 'bidders.index', params: { projectId: this.$store.state.route.params.projectId, quoteId: quote.id } });
        });
    },
    async removeQuoteRequest(quoteRequestId) {
      await services.quoteService.remove(Number(quoteRequestId));
      this.$refs.quoteRequestsTable.reload();
    },
    showRemoveModal() {
      return this.isRemoveModalShown;
    },
    showCategoryModal() {
      return this.isCategoryModalShown;
    },
    setSelectedRow(rowId) {
      this.selectedRow = rowId;
    },
    formLinkToQuote(rowData, name) {
      return { name, params: { projectId: this.$store.state.route.params.projectId, quoteId: rowData.id, quoteName: rowData.quoteName } };
    },
    formatQuoteName(value) {
      if (value) {
        return value;
      }
      return 'Nimetön tarjouspyyntö';
    },
  },
};
