/* ============
 * Inner-files Page
 * ============
 *
 * todo: add documentation here!
 */

export default {
  components: {
    VAppLayout: require('layouts/app-inner-nav/app-inner-nav.vue'),
    VNavFooter: require('components/nav-footer/nav-footer.vue'),
    VBloc: require('components/bloc/bloc.vue'),
    VSectionHeader: require('components/section-header/section-header.vue'),
    VCostRowComponent: require('components/cost-row-component/cost-row-component.vue')
  },
};
