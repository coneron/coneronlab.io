/* ============
 * Inner-basics Page
 * ============
 *
 * todo: add documentation here!
 */

export default {
  components: {
    VAppLayout: require('layouts/app-inner-nav/app-inner-nav.vue'),
    VNavFooter: require('components/nav-footer/nav-footer.vue'),
    VBloc: require('components/bloc/bloc.vue'),
    VFormGroupInput: require('components/form-group-input/form-group-input.vue'),
    VSectionHeader: require('components/section-header/section-header.vue'),
    VAlert: require('components/alert/alert.vue'),
  },
};
