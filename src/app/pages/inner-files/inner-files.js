/* ============
 * Inner-files Page
 * ============
 *
 * todo: add documentation here!
 */
import Dropzone from 'vue2-dropzone';
import * as services from './../../services';
import 'vue2-dropzone/dist/vue2Dropzone.css';

export default {
  components: {
    Dropzone,
    VAppLayout: require('layouts/app-inner-nav/app-inner-nav.vue'),
    VNavFooter: require('components/nav-footer/nav-footer.vue'),
    VBloc: require('components/bloc/bloc.vue'),
    VSectionHeader: require('components/section-header/section-header.vue'),
  },
  data() {
    return {
      dropzoneOptions: {
        url: process.env.API_LOCATION + "/uploads",
        paramName: "uri", 
        addRemoveLinks: true,     
        thumbnailWidth: 150,
        previewTemplate: this.template(),
        dictDefaultMessage: "Pudota tiedostot tähän"
      }
    }
  },
  methods: {
    showSuccess(file, response) {
      console.log('A file was successfully uploaded');
      console.log(file);
      console.log(response);
    },
    template() {
      return `<div class="dz-file-preview row nomargin voffset">
                
              <div class="col-sm-9">
                  <div class="dz-filename"><span data-dz-name></span></div>
                </div>

                <div class="col-sm-3">
                  <a data-dz-remove class="dz-remove-button btn btn-sm pull-right btn-celestial-blue">
                      <span class="ion ion-android-close icon-spacer icon-white"></span>Poista
                  </a>
                </div>

                <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                
          </div>
          `;
    },
    sendingEvent (file, xhr, formData) {
      const quoteId = Number(this.$store.state.route.params.quoteId);      
      formData.append('quoteId', quoteId);
    },
    mountFiles() {
      const quoteId = Number(this.$store.state.route.params.quoteId);
      services.fileService.find({
        query: {
          quoteId,
        },
      }).then((files) => {
        files.data.forEach((file) => {
          const mockFile = { name: file.fileName, id: file.id };
          this.$refs.myDropzone.manuallyAddFile(
            mockFile,
            file.fileName,
            null,
            null,
            {
              dontSubstractMaxFiles: false,
              addToFiles: true,
            });
        });
      });
    },
    removeFile(file) {
      console.log('Removing the file!');
      if (file.id) {
        services.fileService.remove(file.id);
      } else {
        const response = JSON.parse(file.xhr.response);
        services.fileService.remove(response.id);
      }
    },
  },
};
