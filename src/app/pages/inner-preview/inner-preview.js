/* ============
 * Inner-preview Page
 * ============
 *
 * todo: add documentation here!
 */


export default {
  components: {
    VAppLayout: require('layouts/app-inner-nav/app-inner-nav.vue'),
    VNavFooter: require('components/nav-footer/nav-footer.vue'),
    VBloc: require('components/bloc/bloc.vue'),
    VQuotePreviewBody: require('components/quote-preview-body/quote-preview-body.vue'),
    VPanel: require('components/panel/panel.vue'),
  },
  computed: {
    contacts() {
      const contacts = this.$store.state.quote.current.contacts;
      const emails = contacts.map(contact => contact.email);
      if (emails) {
        return emails;
      }
      return false;
    },
  },
};
