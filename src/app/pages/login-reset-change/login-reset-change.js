/* ============
 * Login-reset-change Page
 * ============
 *
 * todo: add documentation here!
 */
import notifierService from './../../services/auth/auth';

export default {
  data() {
    return {
      reset: false,
      user: {
        password: null,
      },
    };
  },
  components: {
    VLayout: require('layouts/minimal/minimal.vue'),
    VBloc: require('components/bloc/bloc.vue'),
  },
  methods: {
    resetPassword(password) {
      notifierService.resetPassword(this.$route.params.token, password).then(() => {
        this.reset = true;
      });
    },
  },

};
