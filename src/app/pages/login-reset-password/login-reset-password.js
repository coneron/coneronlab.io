/* ============
 * Login-reset-password Page
 * ============
 *
 * todo: add documentation here!
 */
import notifierService from './../../services/auth/auth';

export default {
  data() {
    return {
      user: {
        email: null,
      },
    };
  },
  components: {
    VLayout: require('layouts/minimal/minimal.vue'),
    VBloc: require('components/bloc/bloc.vue'),
  },
  methods: {
    sendPassword(email) {
      notifierService.sendResetPassword(email);
    },
    // resetPassword(password) {
    //   notifierService.resetPassword(this.$route.params.slug, password);
    // },
  },
};
