/* ============
 * Make-offer Page
 * ============
 *
 * todo: add documentation here!
 */
import { router } from './../../../bootstrap';
import * as services from './../../services';

export default {
  components: {
    VAppLayout: require('layouts/app/app.vue'),
    VSubHeader: require('components/subheader/subheader.vue'),
    VBloc: require('components/bloc/bloc.vue'),
    VFormGroupInput: require('components/form-group-input/form-group-input.vue'),
    VQuotePreviewBody: require('components/quote-preview-body/quote-preview-body.vue'),
    VPanel: require('components/panel/panel.vue'),
  },
  data: () => ({
    loading: true,
  }),
  created() {
    this.$store.dispatch('getQuote', this.$store.state.route.params.quoteId).then(() => {
      this.loading = false;
    });
  },
  methods: {
    async updateOffer() {
      const organizationId = parseInt(this.$store.state.account.organizationId, 10);
      const quoteId = parseInt(this.$store.state.route.params.quoteId, 10);

      await this.$store.dispatch('updateOffer', [organizationId, 'organizationId']);
      await this.$store.dispatch('updateOffer', [quoteId, 'quoteId']);

      services.offerService.create(this.$store.state.offer.current);
      // TODO: Push different page if logged in
      router.push({ name: 'received-quotes.index' });
    },
  },
};
