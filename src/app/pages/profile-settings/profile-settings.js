/* ============
 * Make-offer Page
 * ============
 *
 * todo: add documentation here!
 */
import { router } from './../../../bootstrap';
import * as services from './../../services';

export default {
  components: {
    VAppLayout: require('layouts/app/app.vue'),
    VSubHeader: require('components/subheader/subheader.vue'),
    VBloc: require('components/bloc/bloc.vue'),
    VFormGroupInput: require('components/form-group-input/form-group-input.vue'),
    VQuotePreviewBody: require('components/quote-preview-body/quote-preview-body.vue'),
    VPanel: require('components/panel/panel.vue'),
  },
  data() {
    return {
      loading: true,
    };
  },
  created() {
    this.fetchData();
  },
  methods: {
    async patchAccount() {
      await services.userService.patch(this.$store.state.account.id, this.$store.state.account);
      await services.organizationService.patch(this.$store.state.account.organizationId, this.$store.state.organization);
      // TODO: Throw error if patching fails
      // TODO: Show success message
      router.push({ name: 'profile-settings.index' });
    },
    fetchData() {
      this.$store.dispatch('getOrganization', this.$store.state.account.organizationId).then(() => {
        this.loading = false;
      });
    },
  },
};
