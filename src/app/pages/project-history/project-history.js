/* ============
 * Projects Page
 * ============
 *
 * todo: add documentation here!
 */
import * as services from './../../services';
import BootstrapStyle from './../../mixins/vuetable-bootstrap-css';
import noDataMixin from './../../mixins/no-data-template';

export default {
  mixins: [noDataMixin],
  components: {
    VAppLayout: require('layouts/app/app.vue'),
    VSubHeader: require('components/subheader/subheader.vue'),
    VVuetableContainer: require('components/vuetable-container/vuetable-container.vue'),
    VSpinner: require('components/spinner/spinner.vue'),
    VFormGroupInput: require('components/form-group-input/form-group-input.vue'),
  },
  data: () => ({
    url: process.env.API_LOCATION,
    css: BootstrapStyle,
    show: false,
    loading: true,
    fields: [
      {
        name: 'projectName',
        title: 'Projekti',
        dataClass: 'p-t-2',
        callback: 'formatEmpty',
      },
      {
        name: 'projectNumber',
        title: 'Projekti NRO',
        dataClass: 'p-t-2',
        callback: 'formatEmpty',
      },
      {
        name: '__slot:actions',
        title: 'Toiminnot',
        titleClass: 'text-right',
        callback: 'formatEmpty',
      },
    ],
  }),
  methods: {
    onLoading() {
      this.loading = false;
    },
    updateProject() {
      if (this.$store.state.account.organizationId && this.$store.state.project.current) {
        const organizationId = parseInt(this.$store.state.account.organizationId, 10);

        this.$store.dispatch('updateProjectOwner', organizationId).then(() => {
          services.projectService.create(this.$store.state.project.current).then(() => {
            this.$nextTick(() => {
              this.$refs.vuetable.refresh();
            });
          });
        });
      }

      this.show = false;
    },
  },
};
