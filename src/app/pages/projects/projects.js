/* ============
 * Projects Page
 * ============
 *
 * todo: add documentation here!
 */
import * as services from './../../services';
import BootstrapStyle from './../../mixins/vuetable-bootstrap-css';
import noDataMixin from './../../mixins/no-data-template';

export default {
  mixins: [noDataMixin],
  components: {
    VAppLayout: require('layouts/app/app.vue'),
    VSubHeader: require('components/subheader/subheader.vue'),
    VVuetableContainer: require('components/vuetable-container/vuetable-container.vue'),
    VModal: require('components/modal/modal.vue'),
    VSpinner: require('components/spinner/spinner.vue'),
    VFormGroupInput: require('components/form-group-input/form-group-input.vue'),
  },
  data: () => ({
    url: process.env.API_LOCATION,
    css: BootstrapStyle,
    show: false,
    isRemoveModalShown: false,
    isEditModalShown: false,
    loading: true,
    selectedRow: null,
    fields: [
      {
        name: '__slot:projectName',
        title: 'Projekti',
        dataClass: 'p-t-2',
        callback: 'formatProjectName',
      },
      {
        name: 'projectNumber',
        title: 'Projekti NRO',
        dataClass: 'p-t-2',
        callback: 'formatEmpty',
      },
      {
        name: 'quotes',
        title: 'Tarjouspyyntöjä',
        dataClass: 'p-t-2',
        callback: 'formatEmpty',
      },
      {
        name: '__slot:actions',
        title: 'Toiminnot',
        titleClass: 'text-right',
        callback: 'formatEmpty',
      },
    ],
  }),
  methods: {
    onLoading() {
      this.loading = false;
    },
    createProject() {
      if (this.$store.state.account.organizationId && this.$store.state.project.new) {
        const organizationId = Number(this.$store.state.account.organizationId);

        this.$store.dispatch('updateProjectOwner', organizationId).then(() => {
          console.log(this.$store.state.project.new);

          services.projectService.create(this.$store.state.project.new).then(() => {
            this.$nextTick(() => {
              this.$refs.projectTable.refresh();
            });
          });
        });
      }
      this.show = false;
    },
    async patchProject() {
      const project = this.$store.state.project.current;
      const projectId = Number(this.$store.state.project.current.id);
      await this.$store.dispatch('patchProject', [projectId, project]);

      this.show = false;
      this.$refs.projectTable.reload();
    },
    setSelectedRow(rowId) {
      this.selectedRow = rowId;
    },
    openEditProject(ProjectId) {
      this.getProject(ProjectId);
      this.isEditModalShown = true;
    },
    async removeProject(ProjectId) {
      await services.projectService.remove(Number(ProjectId));
      this.$refs.projectTable.reload();
    },
    async getProject(ProjectId) {
      this.$store.dispatch('getProject', Number(ProjectId));
    },
    formatProjectName(value) {
      if (value) {
        return value;
      }
      return 'Nimetön projekti';
    },
  },
};
