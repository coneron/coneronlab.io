/* ============
 * Quote Page
 * ============
 *
 * todo: add documentation here!
 */

export default {
  components: {
    VAppLayout: require('layouts/app/app.vue'),
    VBloc: require('components/bloc/bloc.vue'),
    VQuotePreviewBody: require('components/quote-preview-body/quote-preview-body.vue'),
    VPanel: require('components/panel/panel.vue'),
  },
  data() {
    return {
      loading: true,
      error: null,
    };
  },
  created() {
    this.fetchData();
  },
  methods: {
    fetchData() {
      if (this.$store.state.route.params.quoteId) {
        this.$store.dispatch('getQuote', this.$store.state.route.params.quoteId).then(() => {
          this.loading = false;
        });
      }
    },
  },
};
