/* ============
 * Received-offers Page
 * ============
 *
 * todo: add documentation here!
 */
import BootstrapStyle from './../../mixins/vuetable-bootstrap-css';
import noDataMixin from './../../mixins/no-data-template';


export default {
  mixins: [noDataMixin],
  components: {
    VVuetableContainer: require('components/vuetable-container/vuetable-container.vue'),
    VAppLayout: require('layouts/app/app.vue'),
    VSubHeader: require('components/subheader/subheader.vue'),
    VSpinner: require('components/spinner/spinner.vue'),
    VBloc: require('components/bloc/bloc.vue'),
  },
  data: () => ({
    url: process.env.API_LOCATION,
    css: BootstrapStyle,
    params: { sort: '$sort', page: '', perPage: '$limit' },
    loading: true,
    fields: [
      {
        name: 'organization.organizationName',
        title: 'Toimittaja',
        dataClass: 'p-t-2',
        callback: 'formatEmpty',
      },
      {
        name: 'message',
        title: 'Viesti',
        dataClass: 'p-t-2',
        callback: 'formatEmpty',
      },
      {
        name: 'shipmentEndDate',
        title: 'Toimitus pvm.',
        dataClass: 'p-t-2',
        callback: 'formatDate|DD-MM-YYYY',

      },
      {
        name: 'price',
        title: 'Hinta-arvio',
        dataClass: 'p-t-2',
        callback: 'formatEmpty',
      },
      {
        name: '__slot:actions',
        title: 'Toiminnot',
        titleClass: 'text-right',
      },
    ],
  }),
  created() {
    this.$store.dispatch('getQuote', this.$store.state.route.params.quoteId).then(() => {
      this.loading = false;
    });
  },
  methods: {
    onLoading() {
      this.loading = false;
    },
  },
};
