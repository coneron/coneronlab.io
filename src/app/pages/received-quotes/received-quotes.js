/* ============
 * Received-quotes Page
 * ============
 *
 * todo: add documentation here!
 */

import BootstrapStyle from './../../mixins/vuetable-bootstrap-css';
import noDataMixin from './../../mixins/no-data-template';


export default {
  mixins: [noDataMixin],
  components: {
    VVuetableContainer: require('components/vuetable-container/vuetable-container.vue'),
    VAppLayout: require('layouts/app/app.vue'),
    VSubHeader: require('components/subheader/subheader.vue'),
    VSpinner: require('components/spinner/spinner.vue'),
    VBloc: require('components/bloc/bloc.vue'),
  },
  data: () => ({
    url: process.env.API_LOCATION,
    css: BootstrapStyle,
    params: { sort: '$sort', page: '', perPage: '$limit' },
    loading: true,
    fields: [
      {
        name: 'quoteName',
        title: 'Tarjouspyyntö',
        dataClass: 'p-t-2',
        callback: 'formatQuoteName',
      },
      {
        name: 'projects.organizations.organizationName',
        title: 'Tilaaja',
        dataClass: 'p-t-2',
        callback: 'formatEmpty',
      },
      {
        name: 'quoteEndDate',
        title: 'Toimitus pvm.',
        dataClass: 'p-t-2',
        callback: 'formatDate|DD-MM-YYYY',

      },
      {
        name: '__slot:actions',
        title: 'Toiminnot',
        titleClass: 'text-right',
      },
    ],
  }),
  methods: {
    onLoading() {
      this.loading = false;
    },
  },
};
