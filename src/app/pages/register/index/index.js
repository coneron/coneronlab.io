/* ============
 * Register Index Page
 * ============
 *
 * Page where the user can register
 */

import authService from './../../../services/auth';

export default {

  data() {
    return {
      user: {
        firstName: null,
        lastName: null,
        organizationName: null,
        email: null,
        password: null,
        organizationTypeId: 1,
      },
    };
  },

  methods: {
    register(user) {
      authService.register(user);
    },
  },

  components: {
    VLayout: require('layouts/minimal/minimal.vue'),
    VPanel: require('components/panel/panel.vue'),
    VBloc: require('components/bloc/bloc.vue'),
  },
};
