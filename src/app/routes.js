/* ============
 * Routes File
 * ============
 *
 * The routes and redirects are defined in this file
 */


/**
 * The routes
 *
 * @type {object} The routes
 */
export default [
  // Home
  {
    path: '/home',
    name: 'home.index',
    component: require('pages/home/index/index.vue'),

    // If the user needs to be authenticated to view this page
    meta: {
      auth: false,
    },
  },

  // Account
  {
    path: '/account',
    name: 'account.index',
    component: require('pages/account/index/index.vue'),

    // If the user needs to be authenticated to view this page
    meta: {
      auth: true,
    },
  },

  // Login
  {
    path: '/login',
    name: 'login.index',
    component: require('pages/login/index/index.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      guest: true,
    },
  },

  // Register
  {
    path: '/register',
    name: 'register.index',
    component: require('pages/register/index/index.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      guest: true,
    },
  },

  {
    path: '/reset',
    name: 'reset.index',
    component: require('pages/login-reset-password/login-reset-password.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      guest: true,
    },
  },

  {
    path: '/reset/:token',
    name: 'reset-change.index',
    component: require('pages/login-reset-change/login-reset-change.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      guest: true,
    },
  },

  {
    path: '/projects',
    name: 'projects.index',
    component: require('pages/projects/projects.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      auth: true,
    },
  },

  {
    path: '/project-history',
    name: 'project-history.index',
    component: require('pages/project-history/project-history.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      auth: true,
    },
  },

  {
    path: '/project/:projectId/bids-open',
    name: 'bids-open.index',
    component: require('pages/bids-open/bids-open.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      auth: true,
    },
  },
  {
    path: '/project/:projectId/quote/:quoteId/bidders',
    name: 'bidders.index',
    component: require('pages/bidders/bidders.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      auth: true,
    },
  },
  {
    path: '/project/:projectId/quote/:quoteId/inner-basics',
    name: 'inner-basics.index',
    component: require('pages/inner-basics/inner-basics.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      auth: true,
    },
  },
  {
    path: '/project/:projectId/quote/:quoteId/inner-files',
    name: 'inner-files.index',
    component: require('pages/inner-files/inner-files.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      auth: true,
    },
  },
  {
    path: '/project/:projectId/quote/:quoteId/inner-preview',
    name: 'inner-preview.index',
    component: require('pages/inner-preview/inner-preview.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      auth: true,
    },
  },
  {
    path: '/quote/:quoteId',
    name: 'quote.index',
    component: require('pages/quote/quote.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      auth: true,
    },
  },
  {
    path: '/quote/:quoteId/make-offer',
    name: 'make-offer.index',
    component: require('pages/make-offer/make-offer.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      auth: true,
    },
  },
  {
    path: '/received-quotes',
    name: 'received-quotes.index',
    component: require('pages/received-quotes/received-quotes.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      auth: true,
    },
  },
  {
    path: '/project/:projectId/quote/:quoteId/received-offers',
    name: 'received-offers.index',
    component: require('pages/received-offers/received-offers.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      auth: true,
    },
  },

  // Profile settings page
  {
    path: '/profile-settings',
    name: 'profile-settings.index',
    component: require('pages/profile-settings/profile-settings.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      auth: true,
    },
  },

  {
    path: '/project/:projectId/quote/:quoteId/cost-rows',
    name: 'cost-rows.index',
    component: require('pages/cost-rows/cost-rows.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      auth: true,
    },
  },
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/*',
    redirect: '/home',
  },
];
