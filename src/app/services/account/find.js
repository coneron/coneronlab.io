import store from './../../store';
import * as services from './../../services';

// When the request succeeds
const success = (account) => {
  store.dispatch('getAccount', account);
};

// When the request fails
// const failed = () => {};

export default (data) => {
  /*
   * Normally you would perform an AJAX-request.
   * But to get the example working, the data is hardcoded.
   *
   * With the include REST-client Axios, you can do something like this:
   * Vue.$http.get('/account')
   *   .then((response) => {
   *     success(response);
   *   })
   *   .catch((error) => {
   *     failed(error);
   *   });
   */
  if (!data) {
    const accessToken = localStorage.getItem('feathers-jwt');
    // TODO: If not accesstoken
    services.app.passport.verifyJWT(accessToken).then(response =>
    services.app.service('users').get(response.userId)).then((userObj) => {
      success(userObj);
    }).catch((error) => {
      console.error('Error fetching userData!', error);
    });
  } else {
    success({
      firstName: data.firstName,
      lastName: data.lastName,
      id: data.id,
      email: data.email,
      organizationId: data.organizationId, //TODO: Remove hard coding
    });
  }
};
