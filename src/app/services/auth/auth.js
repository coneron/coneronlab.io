import * as services from './../../services';

import Vue from 'vue';

const auth = {
  sendResetPassword: async function sendResetPassword(email) {
    if (!email) {
      Vue.toasted.show('Please provide your email so we can send you a reset password email.');
      return false;
    }

    const options = {
      action: 'sendResetPwd',
      value: { email },
    };

    try {
      await services.authManagementService.create(options);
      Vue.toasted.success('Please check your email. A link to reset your password has been sent.');
    } catch (err) {
      Vue.toasted.info('Error sending reset password email', err);
    }

    return false;
  },
  resetPassword: async function saveResetPassword(slug, password) {
    const options = {
      action: 'resetPwdLong',
      value: { token: slug, password },
    };

    try {
      await services.authManagementService.create(options);
      Vue.toasted.success('Your password was updated. You may now sign in under the new password.');
      return true;
    } catch (err) {
      Vue.toasted.info('Sorry but there was an error updating your password.');
    }

    return false;
  },
};

export default auth;
