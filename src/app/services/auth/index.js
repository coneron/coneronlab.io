import login from './login';
import logout from './logout';
import register from './register';
import auth from './auth';


export default {
  login,
  logout,
  register,
  auth,
};
