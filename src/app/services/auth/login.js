import Vue from 'vue';
import accountService from './../account';
import store from './../../store';
import * as services from './../../services';


// When the request succeeds
const success = (token, userData) => {
  store.dispatch('login', token);
  accountService.find(userData);
  Vue.router.push({
    name: 'projects.index',
  });
};

// When the request fails
const failed = () => {
  Vue.toasted.info('Oho! Unohditko tunnuksesi?');
};

export default (user) => {
  if (!user.email || !user.password) {
    failed();
  } else {
    services.app.authenticate({
      strategy: 'local',
      email: user.email,
      password: user.password,
    }).then(response =>
    // By this point your accessToken has been stored in
    // localstorage
       services.app.passport.verifyJWT(response.accessToken))
    .then(payload => services.app.service('users').get(payload.userId))
    .then((userObj) => {
      services.app.set('user', userObj);
      localStorage.setItem('organizationId', userObj.organizationId);
      success(userObj, services.app.get('user'));
    })
    .catch((error) => {
      Vue.toasted.info('Antamasi sähköpostiosoite tai salasana oli väärin.');
      console.error('Error authenticating!', error);
    });
  }
};
