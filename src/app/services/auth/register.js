import Vue from 'vue';
import store from './../../store';
import * as services from './../../services';

// TODO: Remove registering from admin

// When the request succeeds
const success = (token) => {
  store.dispatch('login', token);
  Vue.router.push({
    name: 'projects.index',
  });
};

// When the request fails
const failed = () => {
  console.log('Request failed.');
};

export default (user) => {
  if (!user.email || !user.password || !user.firstName || !user.lastName) {
    failed();
  } else {
    // TODO: Use transaction
    console.log(user.organizationType);
    services.organizationService.create({
      organizationName: user.organizationName,
      organizationTypeId: user.organizationTypeId,
    }).then((organization) => {
      services.userService.create({
        email: user.email,
        password: user.password,
        firstName: user.firstName,
        lastName: user.lastName,
        organizationId: organization.id,
      });
    })
    .then(() => {
      success();
    }).catch(() => {
      failed();
    });
  }
};
