import axios from 'axios';
import auth from 'feathers-authentication-client';
import feathers from 'feathers/client';
import hooks from 'feathers-hooks';
import rest from 'feathers-rest/client';


const client = feathers();

export const app = client
  .configure(hooks())
  .configure(rest(process.env.API_LOCATION).axios(axios))
  .configure(auth({ storage: window.localStorage }));


export const userService = app.service('users');
export const organizationService = app.service('organizations');
export const projectService = app.service('projects');
export const quoteService = app.service('quotes');
export const fileService = app.service('files');
export const mailQuoteService = app.service('mail-quote');
export const offerService = app.service('offers');
export const authManagementService = app.service('authManagement');
export const categoryService = app.service('service-categories');

