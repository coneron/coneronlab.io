/* ============
 * Vuex Actions
 * ============
 *
 * All the actions that can be used
 * inside the store
 */
import * as types from './mutation-types';
import * as services from '../services';


// Account
export const getAccount = ({ commit }, account) => {
  commit(types.GET_ACCOUNT, account);
};

export const updateAccount = ({ commit }, update) => {
  commit(types.UPDATE_ACCOUNT, update);
};

// Organization
export const updateOrganization = ({ commit }, update) => {
  commit(types.UPDATE_ORGANIZATION, update);
};

export const getOrganization = async ({ commit }, id) => {
  commit(types.GET_ORGANIZATION, await services.organizationService.get(id));
  return Promise.resolve();
};

// Auth
export const login = ({ commit }, token) => {
  commit(types.LOGIN, token);
};

export const logout = ({ commit }) => {
  commit(types.LOGOUT);
};

export const checkAuthentication = ({ commit }) => {
  commit(types.CHECK_AUTHENTICATION);
};

// Quotes
export const updateQuote = ({ commit }, update) => {
  commit(types.UPDATE_QUOTE, update);
};

export const getQuote = async ({ commit }, id) => {
  commit(types.GET_QUOTE, await services.quoteService.get(id));
  return Promise.resolve();
};

// Categories
export const findCategories = async ({ commit }) => {
  commit(types.FIND_CATEGORIES, await services.categoryService.find());
  return Promise.resolve();
};

export const updateQuoteCategories = ({ commit }, update) => {
  commit(types.UPDATE_QUOTE_CATEGORIES, update);
};

// Files
export const findFiles = async ({ commit }, id) => {
  commit(types.FIND_FILES, await services.fileService.find({
    query: {
      quoteId: id,
    },
  }));
  return Promise.resolve();
};

export const removeFile = async ({ commit }, id) => {
  commit(types.REMOVE_FILE, await services.fileService.remove(id));
  return Promise.resolve();
};


// Contacts
export const updateContacts = ({ commit }, update) => {
  commit(types.UPDATE_CONTACTS, update);
};


// Projects
export const createProject = ({ commit }, update) => {
  commit(types.CREATE_PROJECT, update);
};

export const updateProject = ({ commit }, update) => {
  commit(types.UPDATE_PROJECT, update);
};

export const updateProjectOwner = ({ commit }, update) => {
  commit(types.UPDATE_PROJECT_OWNER, update);
};

export const patchProject = async ({ commit }, update) => {
  const [projectId, project] = update;
  const response = await services.projectService.patch(projectId, project);
  commit(types.PATCH_PROJECT, response);
  return Promise.resolve();
};

export const getProject = async ({ commit }, id) => {
  commit(types.GET_PROJECT, await services.projectService.get(id));
  return Promise.resolve();
};


// Offers
export const getOffers = ({ commit }, offers) => {
  commit(types.GET_OFFERS, { offers });
};

export const updateOffer = ({ commit }, update) => {
  commit(types.UPDATE_OFFER, update);
};

