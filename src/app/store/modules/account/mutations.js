import {
  GET_ACCOUNT,
  UPDATE_ACCOUNT,
} from './../../mutation-types';

export default {
  [GET_ACCOUNT](state, account) {
    state.id = account.id;
    state.email = account.email;
    state.firstName = account.firstName;
    state.lastName = account.lastName;
    state.organizationId = account.organizationId;
  },
  [UPDATE_ACCOUNT](state, update) {
    const [value, name] = update;
    state[name] = value;
  },
};
