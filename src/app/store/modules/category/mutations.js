import {
  FIND_CATEGORIES,
} from './../../mutation-types';

export default {
  [FIND_CATEGORIES](state, categories) {
    state.all = categories.data;
  },
};
