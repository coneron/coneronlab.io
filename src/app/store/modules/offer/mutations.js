import {
  UPDATE_OFFER,
  GET_OFFERS,
} from './../../mutation-types';
import Vue from 'vue';

export default {
  [UPDATE_OFFER](state, update) {
    const [value, name] = update;
    state.current[name] = value;
  },
  [GET_OFFERS](state, { offers }) {
    offers.forEach((offer) => {
      if (offer) {
        Vue.set(state, offer.offerId, offer);
      }
    });
  },
};
