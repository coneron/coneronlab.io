import {
  UPDATE_ORGANIZATION,
  GET_ORGANIZATION,
} from './../../mutation-types';

export default {
  [UPDATE_ORGANIZATION](state, update) {
    const [value, name] = update;
    state[name] = value;
  },
  [GET_ORGANIZATION](state, update) {
    state.current = update;
  },
};
