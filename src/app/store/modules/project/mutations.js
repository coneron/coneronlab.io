import {
  CREATE_PROJECT,
  UPDATE_PROJECT,
  GET_PROJECT,
  UPDATE_PROJECT_OWNER,
  PATCH_PROJECT,
} from './../../mutation-types';

export default {
  [CREATE_PROJECT](state, update) {
    const [value, name] = update;
    state.new[name] = value;
  },
  [UPDATE_PROJECT](state, update) {
    const [value, name] = update;
    state.current[name] = value;
  },
  [UPDATE_PROJECT_OWNER](state, update) {
    state.new.organizationId = update;
  },
  [PATCH_PROJECT](state, update) {
    state.current = update;
  },
  [GET_PROJECT](state, update) {
    state.current = update;
  },
};
