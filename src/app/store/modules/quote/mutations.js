import {
  UPDATE_QUOTE,
  GET_QUOTE,
  UPDATE_CONTACTS,
  FIND_FILES,
  REMOVE_FILE,
  UPDATE_QUOTE_CATEGORIES,
} from './../../mutation-types';
import Vue from 'vue';

export default {
  [UPDATE_QUOTE](state, update) {
    const [value, name] = update;
    state.current[name] = value;
  },
  [GET_QUOTE](state, update) {
    state.current = update;
  },
  [UPDATE_CONTACTS](state, contacts) {
    state.current.contacts = [];
    contacts.forEach((contact) => {
      state.current.contacts.push({ email: contact });
    });
  },
  [UPDATE_QUOTE_CATEGORIES](state, category) {
    console.log(category);
    state.current.categories.push({ id: category });
  },
  [FIND_FILES](state, files) {
    state.files = files.data;
  },
  [REMOVE_FILE](state, file) {
    const index = state.files.findIndex(set => set.id === file.id);
    Vue.delete(state.files, index);
  },
};
