// Account
export const GET_ACCOUNT = 'GET_ACCOUNT';
export const UPDATE_ACCOUNT = 'UPDATE_ACCOUNT';

// Organization
export const UPDATE_ORGANIZATION = 'UPDATE_ORGANIZATION';
export const GET_ORGANIZATION = 'GET_ORGANIZATION';

// Auth
export const CHECK_AUTHENTICATION = 'CHECK_AUTHENTICATION';
export const LOGOUT = 'LOGOUT';
export const LOGIN = 'LOGIN';

// Quote
export const UPDATE_QUOTE = 'UPDATE_QUOTE';
export const GET_QUOTE = 'GET_QUOTE';

// Categories
export const FIND_CATEGORIES = 'FIND_CATEGORIES';
export const UPDATE_QUOTE_CATEGORIES = 'UPDATE_QUOTE_CATEGORIES';

// Files
export const FIND_FILES = 'FIND_FILES';
export const REMOVE_FILE = 'REMOVE_FILE';

// Contacts
export const UPDATE_CONTACTS = 'UPDATE_CONTACTS';

// Project
export const CREATE_PROJECT = 'CREATE_PROJECT';
export const UPDATE_PROJECT = 'UPDATE_PROJECT';
export const UPDATE_PROJECT_OWNER = 'UPDATE_PROJECT_OWNER';
export const PATCH_PROJECT = 'PATCH_PROJECT';
export const GET_PROJECT = 'GET_PROJECT';

// Offers
export const GET_OFFERS = 'GET_OFFERS';
export const UPDATE_OFFER = 'UPDATE_OFFER';
